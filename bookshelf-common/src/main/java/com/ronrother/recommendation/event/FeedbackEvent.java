package com.ronrother.recommendation.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class FeedbackEvent {

    public enum Action {
        ADDED, REMOVED
    }

    public enum Type {
        LIKE, DISLIKE
    }

    private Long isbn;
    private String user;
    private Type type;
    private Action action;

}
