package com.ronrother.recommendation.event;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface Channels {

    @Input("feedback")
    SubscribableChannel feedback();

}
