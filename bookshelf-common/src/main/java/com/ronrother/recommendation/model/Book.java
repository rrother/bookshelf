package com.ronrother.recommendation.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Entity(name = "book")
public class Book {

    @Id
    @Column(columnDefinition = "NUMERIC")
    private Long id;

    private String title;
    private String author;
    private String genre;

    @Column(columnDefinition = "NUMERIC")
    private Long likes;

    @Column(columnDefinition = "NUMERIC")
    private Long dislikes;

}
