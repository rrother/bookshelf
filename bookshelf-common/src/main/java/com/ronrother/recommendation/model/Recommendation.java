package com.ronrother.recommendation.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Entity(name = "recommendation")
public class Recommendation {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    @Column(columnDefinition = "NUMERIC")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "book_id")
    private Book book;

    private String username;
}
