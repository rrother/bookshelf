package com.ronrother.recommendation.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Entity(name = "feedback")
public class Feedback {

    public enum Type {
        LIKE, DISLIKE
    }

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    @Column(columnDefinition = "NUMERIC")
    private Long id;

    private String username;
    private String type;

    @ManyToOne
    @JoinColumn(name = "book_id")
    private Book book;

}
