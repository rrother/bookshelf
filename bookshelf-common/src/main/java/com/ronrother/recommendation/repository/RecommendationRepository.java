package com.ronrother.recommendation.repository;

import com.ronrother.recommendation.model.Recommendation;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface RecommendationRepository extends CrudRepository<Recommendation, Long> {

    List<Recommendation> findAllByUsername(String username);

    @Transactional
    void deleteByUsername(String username);

}
