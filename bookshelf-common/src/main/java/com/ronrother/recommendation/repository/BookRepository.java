package com.ronrother.recommendation.repository;

import com.ronrother.recommendation.model.Book;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BookRepository extends CrudRepository<Book, Long> {

    List<Book> findByOrderByLikesDesc(Pageable pageable);

    List<Book> findByAuthorInOrderByLikesDesc(List<String> author, Pageable pageable);

    List<Book> findByGenreInOrderByLikesDesc(List<String> genres, Pageable of);
}