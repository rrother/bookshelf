package com.ronrother.recommendation.repository;

import com.ronrother.recommendation.model.Author;
import com.ronrother.recommendation.model.Genre;
import com.ronrother.recommendation.model.Feedback;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FeedbackRepository extends CrudRepository<Feedback, Long> {

    Feedback findByUsernameAndBookId(String username, Long bookId);

    Long countByBookIdAndType(Long bookId, String type);

    @Query("select new com.ronrother.recommendation.model.Author(b.author) " +
            "from feedback a " +
            "inner join book b " +
            "on a.book = b.id " +
            "where a.username = ?1 " +
            "group by b.author " +
            "order by count(b.author) desc")
    List<Author> findAuthorsByUsernameOrderByCountDesc(String username, Pageable pageable);

    @Query("select new com.ronrother.recommendation.model.Genre(b.genre) " +
            "from feedback a " +
            "inner join book b " +
            "on a.book = b.id " +
            "where a.username = ?1 " +
            "group by b.genre " +
            "order by count(b.genre) desc")
    List<Genre> findGenresByUsernameOrderByCountDesc(String username, Pageable pageable);

}
