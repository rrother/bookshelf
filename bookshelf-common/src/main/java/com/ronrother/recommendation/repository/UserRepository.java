package com.ronrother.recommendation.repository;

import com.ronrother.recommendation.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

}