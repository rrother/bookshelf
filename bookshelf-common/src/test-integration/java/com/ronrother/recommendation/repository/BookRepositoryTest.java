package com.ronrother.recommendation.repository;

import com.ronrother.recommendation.model.Book;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BookRepositoryTest {

    @Autowired
    BookRepository bookRepository;

    @Before
    public void init() {
        bookRepository.deleteAll();
        bookRepository.save(Book.builder()
                .id(374502005L)
                .author("Bernard Malamud")
                .title("The Natural")
                .genre("Science Fiction & Fantasy")
                .build());
        bookRepository.save(Book.builder()
                .id(395366119L)
                .author("Bill Peet")
                .title("Eli")
                .genre("Science Fiction & Fantasy")
                .build());
        bookRepository.save(Book.builder()
                .id(140094296L)
                .author("Bruce Chatwin")
                .title("The Songlines")
                .genre("History")
                .build());
    }

    @Test
    public void shouldFindBooks() {
        assertEquals(3, bookRepository.findByOrderByLikesDesc(PageRequest.of(0, 20)).size());
    }

    @Test
    public void shouldPreferPopularBooks() {
        Book book = bookRepository.findById(140094296L).get();
        book.setLikes(1L);
        bookRepository.save(book);
        assertEquals(
                "The Songlines",
                bookRepository.findByOrderByLikesDesc(PageRequest.of(0, 1)).get(0).getTitle()
        );
    }

    @Test
    public void shouldFindPopularBooksFromAuthor() {
        Book book = bookRepository.findById(140094296L).get();
        book.setLikes(1L);
        bookRepository.save(book);

        book = bookRepository.findById(395366119L).get();
        book.setLikes(10L);
        bookRepository.save(book);

        assertEquals(
                "The Songlines",
                bookRepository.findByAuthorInOrderByLikesDesc(Arrays.asList("Bruce Chatwin"), PageRequest.of(0, 1)).get(0).getTitle()
        );
    }

    @Test
    public void shouldFindPopularBooksFromGenre() {
        Book book = bookRepository.findById(140094296L).get();
        book.setLikes(1L);
        bookRepository.save(book);

        book = bookRepository.findById(395366119L).get();
        book.setLikes(10L);
        bookRepository.save(book);

        assertEquals(
                "The Songlines",
                bookRepository.findByGenreInOrderByLikesDesc(Arrays.asList("History"), PageRequest.of(0, 1)).get(0).getTitle()
        );
    }

}
