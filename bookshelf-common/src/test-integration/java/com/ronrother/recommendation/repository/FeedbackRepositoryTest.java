package com.ronrother.recommendation.repository;

import com.ronrother.recommendation.model.Book;
import com.ronrother.recommendation.model.Feedback;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FeedbackRepositoryTest {

    @Autowired
    FeedbackRepository feedbackRepository;

    @Autowired
    BookRepository bookRepository;

    @Before
    public void init() {
        feedbackRepository.deleteAll();
        bookRepository.deleteAll();
        List<Book> books = Arrays.asList(
                Book.builder()
                        .id(374502005L)
                        .author("Bernard Malamud")
                        .title("The Natural")
                        .genre("Science Fiction & Fantasy")
                        .likes(10L)
                        .build(),
                Book.builder()
                        .id(395366119L)
                        .author("Bernard Malamud")
                        .title("Eli")
                        .genre("Science Fiction & Fantasy")
                        .build(),
                Book.builder()
                        .id(140094296L)
                        .author("Bruce Chatwin")
                        .title("The Songlines")
                        .genre("History")
                        .likes(1L)
                        .build()
        );
        books.forEach(bookRepository::save);

        feedbackRepository.save(Feedback.builder()
                .book(books.get(0))
                .id(1L)
                .username("demo")
                .type("LIKE")
                .build()
        );
        feedbackRepository.save(Feedback.builder()
                .book(books.get(1))
                .id(2L)
                .username("demo")
                .type("LIKE")
                .build()
        );
        feedbackRepository.save(Feedback.builder()
                .book(books.get(1))
                .id(3L)
                .username("user1")
                .type("LIKE")
                .build()
        );
    }

    @Test
    public void shouldFindByUsernameAndBookId() {
        assertNotNull(feedbackRepository.findByUsernameAndBookId("demo", 374502005L));
    }

    @Test
    public void shouldCountByBookIdAndType() {
        assertEquals(2L, feedbackRepository.countByBookIdAndType(395366119L, "LIKE").longValue());
    }

    @Test
    public void shouldFindUserFavoriteAuthor() {
        assertEquals(
                "Bernard Malamud",
                feedbackRepository.findAuthorsByUsernameOrderByCountDesc("demo", PageRequest.of(0, 1)).get(0).getName()
        );
    }

    @Test
    public void shouldFindUserFavoriteGenre() {
        assertEquals(
                "Science Fiction & Fantasy",
                feedbackRepository.findGenresByUsernameOrderByCountDesc("demo", PageRequest.of(0, 1)).get(0).getName()
        );
    }

}
