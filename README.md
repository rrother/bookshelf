# Proposal

## Architecture

The proposed architecture consists of two small services, one two collect user feedback via like/dislike endpoints,
and another to generate recommendations reacting to the events issued by the first, and to serve them via its own
endpoint.

Spring Cloud Stream with Apache Kafka were used to provide the event source. Spring Boot was used as the framework for building both services.Persistence is handled via Spring Data, and Postgres is 
used as database.

**Note:** Despite having been delivered as two separate services (bookshelf-recommendation and bookshelf-navigation), both
services share the same database and data model, making them effectively one service.   

## Recommendation

The recommendation algorithm is very simple. At first, the user gets the 20 books with the highest number of likes,
or essentially random books if no "feedback" record exists in the database.

As the user "likes" one or more books, the recommendations will return first (up to 10) books from the same author,
sorted by their total number of likes, followed by (up to 10) books from the same genre, also sorted by their number
or likes. The listed is completed with the globally most liked books to fill the required number of 20 recommendations.

# Building

Docker is a prerequisite to run build and run the projects.

Open a terminal in the project folder and issue the following command:

```bash
./gradlew assemble docker
```

#Running

After building the project, open a terminal in the project folder and issue the following command:

```bash
docker-compose up
``` 

# Opportunities for improvement

This project needs improvements in many fronts, such as:

* Better recommendations, maybe using a clustering algorithm to identify similar books
* Testing is lacking
* Configuration is hardcoded in the application.properties files
* Authentication needs to be implemented (currently using a fixed username)