package com.ronrother.recommendation;

import com.ronrother.recommendation.model.Feedback;
import com.ronrother.recommendation.service.BookshelfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for the bookshelf navigation service API.
 */
@RestController
@RequestMapping(value = "/v1/book/{id}")
public class BookshelfController {

    private BookshelfService bookshelfService;

    @Autowired
    public BookshelfController(BookshelfService bookshelfService) {
        this.bookshelfService = bookshelfService;
    }

    @RequestMapping(value = "/like", method = RequestMethod.PUT)
    public void like(@PathVariable Long id) {
        bookshelfService.registerFeedback(id, Feedback.Type.LIKE);
    }

    @RequestMapping(value = "/like", method = RequestMethod.DELETE)
    public void unlike(@PathVariable Long id) {
        bookshelfService.removeBookFeedback(id);
    }

    @RequestMapping(value = "/dislike", method = RequestMethod.PUT)
    public void dislike(@PathVariable Long id) {
        bookshelfService.registerFeedback(id, Feedback.Type.DISLIKE);
    }

    @RequestMapping(value = "/dislike", method = RequestMethod.DELETE)
    public void undislike(@PathVariable Long id) {
        bookshelfService.removeBookFeedback(id);
    }

}
