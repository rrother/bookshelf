package com.ronrother.recommendation.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
public class FeedbackEventSource {

    private Source source;

    @Autowired
    public FeedbackEventSource(Source source) {
        this.source = source;
    }

    public void publishFeedbackEvent(FeedbackEvent event) {
        source.output().send(
                MessageBuilder.withPayload(event).build()
        );
    }

}
