package com.ronrother.recommendation.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.function.Supplier;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason="Book not found")
public class BookDoesNotExistException extends RuntimeException {
}
