package com.ronrother.recommendation.service;

import com.ronrother.recommendation.event.FeedbackEvent;
import com.ronrother.recommendation.event.FeedbackEventSource;
import com.ronrother.recommendation.model.Book;
import com.ronrother.recommendation.model.Feedback;
import com.ronrother.recommendation.repository.BookRepository;
import com.ronrother.recommendation.repository.FeedbackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service that handles bookshelf navigation.
 */
@Service
public class BookshelfService {

    //TODO: Proper authentication
    private static final String FIXED_USER_NAME = "demo";

    private BookRepository bookRepository;
    private FeedbackRepository feedbackRepository;
    private FeedbackEventSource feedbackSource;

    @Autowired
    public BookshelfService(FeedbackRepository feedbackRepository, FeedbackEventSource feedbackSource,
                            BookRepository bookRepository) {
        this.bookRepository = bookRepository;
        this.feedbackRepository = feedbackRepository;
        this.feedbackSource = feedbackSource;
    }

    /**
     * Registers a feedback for a book, associated with the current user.
     * @param id the book Id
     * @param type the feedback type
     * @throws BookDoesNotExistException if the referenced book does not exist
     */
    public void registerFeedback(Long id, Feedback.Type type) throws BookDoesNotExistException {
        Book book = bookRepository.findById(id).orElseThrow(BookDoesNotExistException::new);
        deleteBookFeedback(book);
        saveBookFeedback(book, type);
        updateBookFeedbackSummary(book);

        feedbackSource.publishFeedbackEvent(
                FeedbackEvent.builder()
                        .isbn(id)
                        .type(type == Feedback.Type.LIKE ? FeedbackEvent.Type.LIKE : FeedbackEvent.Type.DISLIKE)
                        .user(FIXED_USER_NAME)
                        .action(FeedbackEvent.Action.ADDED)
                        .build()
        );
    }

    /**
     * Registers a feedback for a book, associated with the current user.
     * @param id the book Id
     * @throws BookDoesNotExistException if the referenced book does not exist
     */
    public void removeBookFeedback(Long id) throws BookDoesNotExistException {
        Book book = bookRepository.findById(id).orElseThrow(BookDoesNotExistException::new);
        deleteBookFeedback(book);
        updateBookFeedbackSummary(book);

        feedbackSource.publishFeedbackEvent(
                FeedbackEvent.builder()
                        .isbn(id)
                        .user(FIXED_USER_NAME)
                        .action(FeedbackEvent.Action.REMOVED)
                        .build()
        );
    }

    private void deleteBookFeedback(Book book) {
        Feedback Feedback = feedbackRepository.findByUsernameAndBookId(FIXED_USER_NAME, book.getId());
        if (Feedback != null) {
            feedbackRepository.delete(Feedback);
        }
    }

    private void saveBookFeedback(Book book, Feedback.Type type) {
        feedbackRepository.save(
                Feedback.builder()
                        .username(FIXED_USER_NAME)
                        .book(book)
                        .type(type.toString())
                        .build()
        );
    }

    private void updateBookFeedbackSummary(Book book) {
        book.setLikes(feedbackRepository.countByBookIdAndType(book.getId(), Feedback.Type.LIKE.toString()));
        book.setDislikes(feedbackRepository.countByBookIdAndType(book.getId(), Feedback.Type.DISLIKE.toString()));
        bookRepository.save(book);
    }

}
