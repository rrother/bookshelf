package com.ronrother.recommendation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.palantir.docker.compose.DockerComposeRule;
import com.palantir.docker.compose.connection.DockerPort;
import com.palantir.docker.compose.connection.waiting.HealthChecks;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

import static io.restassured.RestAssured.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class End2EndTest {

    private static final ObjectMapper JACKSON = new ObjectMapper();

    @ClassRule
    public static DockerComposeRule docker = DockerComposeRule.builder()
            .file("src/test-e2e/resources/docker-compose.yaml")
            .waitingForService("recommendation", HealthChecks.toRespondOverHttp(8081, (port) -> port.inFormat("http://$HOST:$EXTERNAL_PORT")))
            .waitingForService("navigation", HealthChecks.toRespondOverHttp(8080, (port) -> port.inFormat("http://$HOST:$EXTERNAL_PORT")))
            .build();

    private DockerPort navigationService = docker.containers()
            .container("navigation")
            .port(8080);

    private DockerPort recommendationService = docker.containers()
            .container("recommendation")
            .port(8081);

    @Test
    public void A_shouldReturn20Recommendations() throws IOException {
        Response response = get("recommendations", recommendationService.getExternalPort())
                .extract().response();
        assertEquals(20, JACKSON.readValue(response.asString(), List.class).size());
    }

    @Test
    public void B_shouldBeAbleToSubmitFeedback() throws IOException, InterruptedException {
        Thread.sleep(3000);
        put("book/395349249/like", "", navigationService.getExternalPort())
                .statusCode(200);
    }

    @Test
    public void C_shouldRecommendBooksFromSameAuthorFirst() throws IOException, InterruptedException {
        //Wait for event to be processed
        Thread.sleep(3000);
        Response response = get("recommendations", recommendationService.getExternalPort())
                .extract().response();
        List<Map> recommendations = JACKSON.readValue(response.asString(), List.class);
        assertEquals("Bill Peet", recommendations.get(0).get("author"));
    }

    private ValidatableResponse put(String path, String content, Integer port) {
        return given()
                .body(content)
                .put(String.format("http://localhost:%s/v1/%s", port, path))
                .then();
    }

    private ValidatableResponse get(String path, Integer port) {
        return when()
                .get(String.format("http://localhost:%s/v1/%s", port, path))
                .then();
    }

}
