#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER bookshelf;
    CREATE DATABASE bookshelf;
    GRANT ALL PRIVILEGES ON DATABASE bookshelf TO bookshelf;
EOSQL