package com.ronrother.recommendation;

import com.ronrother.recommendation.model.Recommendation;
import lombok.Data;

@Data
public class RecommendationResource {

    private Long bookId;
    private String title;
    private String author;
    private String genre;

    public RecommendationResource(Recommendation recommendation) {
        this.bookId = recommendation.getBook().getId();
        this.title = recommendation.getBook().getTitle();
        this.author = recommendation.getBook().getAuthor();
        this.genre = recommendation.getBook().getGenre();
    }

}
