package com.ronrother.recommendation.event;

import com.ronrother.recommendation.service.RecommendationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

@EnableBinding(Channels.class)
public class FeedbackHandler {

    private RecommendationService recommendationService;

    @Autowired
    public FeedbackHandler(RecommendationService recommendationService) {
        this.recommendationService = recommendationService;
    }

    @StreamListener("feedback")
    public void feedbackEventSink(FeedbackEvent event) {
        recommendationService.updateUserRecommendations(event.getUser());
    }

}
