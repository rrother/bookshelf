package com.ronrother.recommendation;

import com.ronrother.recommendation.service.RecommendationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/v1/recommendations")
public class RecommendationController {

    private static final int MAXIMUM_RECOMMENDATION_AMOUNT = 20;

    private RecommendationService recommendationService;

    @Autowired
    public RecommendationController(RecommendationService recommendationService) {
        this.recommendationService = recommendationService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<RecommendationResource> getBookRecommendations() {
        return recommendationService.getRecommendationsForUser().stream()
                .limit(MAXIMUM_RECOMMENDATION_AMOUNT)
                .map(RecommendationResource::new)
                .collect(Collectors.toList());
    }

}
