package com.ronrother.recommendation.scheduled;

import com.ronrother.recommendation.repository.UserRepository;
import com.ronrother.recommendation.service.RecommendationService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Scheduled action to update book recommendations for all registered users.
 */
@Component
public class RecommendationUpdater {

    private UserRepository userRepository;
    private RecommendationService recommendationService;

    public RecommendationUpdater(UserRepository userRepository, RecommendationService recommendationService) {
        this.userRepository = userRepository;
        this.recommendationService = recommendationService;
    }

    @Scheduled(initialDelay = 0, fixedRate = 600000)
    public void updateRecommendations() {
        System.out.println("scheduled action");
        userRepository.findAll().forEach(user -> recommendationService.updateUserRecommendations(user.getName()));
    }

}
