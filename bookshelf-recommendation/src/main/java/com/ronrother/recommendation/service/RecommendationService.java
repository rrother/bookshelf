package com.ronrother.recommendation.service;

import com.ronrother.recommendation.model.Author;
import com.ronrother.recommendation.model.Book;
import com.ronrother.recommendation.model.Genre;
import com.ronrother.recommendation.model.Recommendation;
import com.ronrother.recommendation.repository.BookRepository;
import com.ronrother.recommendation.repository.FeedbackRepository;
import com.ronrother.recommendation.repository.RecommendationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RecommendationService {

    //TODO: Proper authentication
    private static final String FIXED_USER_NAME = "demo";

    private BookRepository bookRepository;
    private FeedbackRepository feedbackRepository;
    private RecommendationRepository recommendationRepository;

    @Autowired
    public RecommendationService(BookRepository bookRepository, FeedbackRepository feedbackRepository,
                       RecommendationRepository recommendationRepository) {
        this.bookRepository = bookRepository;
        this.feedbackRepository = feedbackRepository;
        this.recommendationRepository = recommendationRepository;
    }


    public List<Recommendation> getRecommendationsForUser() {
        return new ArrayList<>(recommendationRepository.findAllByUsername(FIXED_USER_NAME));
    }

    public void updateUserRecommendations(String user) {
        recommendationRepository.deleteByUsername(user);

        List<Book> recommendedBooks = new ArrayList<>();
        recommendedBooks.addAll(findBooksFromUserFavoriteAuthors(user));
        recommendedBooks.addAll(findBooksFromUserFavoriteGenres(user));
        recommendedBooks.addAll(findGloballyFavoriteBooks());

        saveRecommendations(recommendedBooks, user);
    }

    private List<Book> findBooksFromUserFavoriteAuthors(String user) {
        List<Author> userFavoriteAuthors = feedbackRepository.findAuthorsByUsernameOrderByCountDesc(
                user, PageRequest.of(0, 5)
        );
        return bookRepository.findByAuthorInOrderByLikesDesc(
                userFavoriteAuthors.stream().map(Author::getName).collect(Collectors.toList()),
                PageRequest.of(0, 10)
        );
    }

    private List<Book> findBooksFromUserFavoriteGenres(String user) {
        List<Genre> userFavoriteGenres = feedbackRepository.findGenresByUsernameOrderByCountDesc(
                user, PageRequest.of(0, 5)
        );
        return bookRepository.findByGenreInOrderByLikesDesc(
                userFavoriteGenres.stream().map(Genre::getName).collect(Collectors.toList()),
                PageRequest.of(0, 10)
        );
    }

    private List<Book> findGloballyFavoriteBooks() {
        return bookRepository.findByOrderByLikesDesc(PageRequest.of(0, 20));
    }

    private void saveRecommendations(List<Book> recommendedBooks, String user) {
        recommendedBooks.stream()
                .distinct()
                .limit(20)
                .forEach(book ->
                        recommendationRepository.save(Recommendation.builder()
                                .book(book)
                                .username(user)
                                .build())
                );
    }


}
